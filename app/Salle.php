<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Salle extends Model
{
  protected $fillable = [
        'description',
        'etat',
        'nom_salle' ]; 

public function reservation(){
    return $this->hasMany(Reservation::class);
}}
