<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Utilisateur extends Model
{
    protected $fillable = [
        'administrateur',
        'login',
        'nom_societe'
];
public function reservation(){
    return $this->hasMany(Reservation::class);

}}
